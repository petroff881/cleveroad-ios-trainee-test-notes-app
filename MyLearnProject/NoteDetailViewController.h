//
//  NoteDetailViewController.h
//  MyLearnProject
//
//  Created by Anton Petrov on 11/21/16.
//  Copyright © 2016 Anton Petrov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteDetailViewController : UIViewController

@property(nonatomic) NSString* passedTitle;
@property(nonatomic) NSString* passedText;

@end
